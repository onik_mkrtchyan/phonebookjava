#!/bin/bash

rm -r target

mkdir target

cd src

javac -d ../target/ ContactMain.java

cd ../target/

rm -r ../src

java ContactMain