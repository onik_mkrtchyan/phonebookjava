## Phone Book Implemented with core Java (My first project, Jan 2021) 

### Implemented CRUD Operations using OOP, Collections, Validations, Exception handling and other base components.

## Functionality

### • create contact
### • get contact(s) by FirstName
### • get contact(s) by FirstName and LastName
### • get contact by Phone number
### • get contact by Email address
### • get contact(s) by Group type
### • get contact(s) by Country
### • get contact(s) by City
### • get contact(s) by City
### • get ALL contacts
### • update contact
### • delete contact by phone number or email address