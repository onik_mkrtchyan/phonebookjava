FROM openjdk:17-alpine3.14

WORKDIR /

COPY . .

#COPY docker.entrypoint.sh .

RUN chmod +x docker.entrypoint.sh

ENTRYPOINT ["/bin/sh","./docker.entrypoint.sh"]